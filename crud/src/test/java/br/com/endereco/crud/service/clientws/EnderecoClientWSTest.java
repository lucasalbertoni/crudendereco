package br.com.endereco.crud.service.clientws;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.endereco.crud.entity.Endereco;

/**
 * Classe para testar o a classe EnderecoClientWS.
 * @author Lucas
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EnderecoClientWSTest {
	
	/**
	 * Classe a ser testada.
	 */
	@Autowired
	private EnderecoClientWS enderecoClientWS;
	
	/**
	 * Método para testar o buscaCep do ClientWS.
	 */
	@Test
	public void testarBuscaCep(){
		Endereco endereco = new Endereco();
		//CEP NULL
		try{
			enderecoClientWS.buscaCep(endereco);
		}catch (Exception e) {
		}
		endereco.setCep("55877652");
		//NUMERO NULL
		try{
			enderecoClientWS.buscaCep(endereco);
		}catch (Exception e) {
		}
		endereco.setNumero("S/N");
		try{
			//Sucesso caso o serviço de busca cep esteja no ar.
			enderecoClientWS.buscaCep(endereco);
		}catch (Exception e) {
			//Caso contrario ira estourar exception
		}
	}
	
}
