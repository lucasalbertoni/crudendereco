package br.com.endereco.crud;

import static org.assertj.core.api.BDDAssertions.then;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.endereco.crud.entity.Endereco;

/**
 * Classe criada para testar as requisições da aplicação e seus devidos retornos.
 * <br /> Para o teste de CRUD funcionar deve-se ter configurado e funcionando o serviço de busca cep.
 * <br /> Essa é uma requisição REAL, logo será cadastrado e excluído um item da base de dados.
 * 
 * @author Lucas
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port=0"})
public class ApplicationConfigTest {
	
	/**
	 * Porta da aplicação.
	 */
	@LocalServerPort
	private int port;

	/**
	 * Template utilizado para testes REST.
	 */
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	/**
	 * URL base para testes.
	 */
	private String urlBase = "http://localhost:";
	
	/**
	 * Caminho base do serviço a ser testado.
	 */
	private String serviceBase = "/endereco";
	
	/**
	 * Parâmetro base a ser utilizado na requisição do serviço.
	 */
	private String parameter = "/";
	
	/**
	 * Método criado para testar a INCLUSAO, ALTERACAO e EXCLUSAO do endereço.
	 * <br /> é como pressuposto que o serviço de busca cep esteja no ar.
	 * @throws Exception
	 */
	@Test
	public void efetuarCrudCompleto() throws Exception {
		//Inclusao
		Endereco endereco = new Endereco();
		endereco.setCep("47100970");
		endereco.setNumero("55");
		URI uri = new URI(this.urlBase + this.port + serviceBase);
		ResponseEntity<Endereco> response = this.testRestTemplate.postForEntity(uri, endereco, Endereco.class);
		then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		//Consulta
		response = this.testRestTemplate.getForEntity(this.urlBase + this.port + serviceBase + parameter + response.getBody().getId(), Endereco.class);
		endereco = response.getBody();
		then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		//Alteracao
		endereco.setCep("04855-990");
		this.testRestTemplate.put(uri, endereco);
		
		//Delecao
		this.testRestTemplate.delete(this.urlBase + this.port + serviceBase + parameter + response.getBody().getId(), endereco.getId());
	}
	
	/**
	 * Criado para testar o retorno de todos enderecos
	 * @throws Exception
	 */
	@Test
	public void retonarListEndereco() throws Exception {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Iterable> entity = this.testRestTemplate.getForEntity(this.urlBase + this.port + serviceBase, Iterable.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

}
