package br.com.endereco.crud.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.endereco.crud.controller.dto.Cep;
import br.com.endereco.crud.entity.Endereco;
import br.com.endereco.crud.service.clientws.EnderecoClientWS;
import br.com.endereco.crud.service.dao.EnderecoDao;

/**
 * Classe criada para testar os serviços de endereço.
 * @author Lucas
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EnderecoServiceTest {

	/**
	 * Atributo do service que será testado.
	 */
	@Autowired
	private EnderecoService enderecoService;
	
	/**
	 * Mock para testar os servicos de endereços.
	 */
	@MockBean
	private EnderecoDao enderecoDao;
	
	/**
	 * Mock para testar os servicos de endereços.
	 */
	@MockBean
	private EnderecoClientWS enderecoClientWS;
	
	/**
	 * Método criado para testar findAll do service.
	 */
	@Test
	public void testarFindAll(){
		Iterable<Endereco> enderecos = new ArrayList<Endereco>();
		given(this.enderecoDao.findAll()).willReturn(enderecos);
		assertThat(enderecos).isEqualTo(enderecoService.findAll());
	}
	
	/**
	 * Método criado para testar o findOne do service.
	 * @param id
	 */
	@Test
	public void testarFindOne(){
		Endereco endereco = new Endereco();
		endereco.setId(1l);
		given(this.enderecoDao.findOne(Mockito.anyLong())).willReturn(endereco);
		assertThat(endereco).isEqualTo(enderecoService.findOne(1l));
	}
	
	/**
	 * Método criado para testar o save do service.
	 * @param endereco
	 */
	@Test
	public void testarSave(){
		Endereco endereco = mocarSaveOrUpdate();
		endereco.setId(null);
		assertThat(enderecoService.save(endereco).getId()).isEqualTo(1l);
	}
	
	/**
	 * Método criado para testar o update do service.
	 * @param endereco
	 */
	@Test
	public void testarUpdate(){
		Endereco endereco = mocarSaveOrUpdate();
		assertThat(endereco).isEqualTo(enderecoService.update(endereco));
	}
	
	/**
	 * método criado para testar o delete do service.
	 * @param id
	 */
	@Test
	public void testarDelete(){
		willDoNothing().given(enderecoDao).delete(Mockito.anyLong());
		assertThat(1l).isEqualTo(enderecoService.delete(1l));
	}

	/**
	 * Método para mocar atributos que são utilizados no save e no update.
	 * @return
	 */
	private Endereco mocarSaveOrUpdate() {
		Endereco endereco = new Endereco();
		endereco.setId(1l);
		endereco.setCep("111111111");
		endereco.setCidade("Teste");
		endereco.setEstado("SP");
		endereco.setLogradouro("Avenida Teste");
		Cep cep = new Cep();
		given(this.enderecoClientWS.buscaCep(Mockito.any(Endereco.class))).willReturn(cep);
		Endereco endereco2 = new Endereco();
		endereco2.setId(1l);
		given(this.enderecoDao.save(Mockito.any(Endereco.class))).willReturn(endereco2);
		return endereco;
	}
}
