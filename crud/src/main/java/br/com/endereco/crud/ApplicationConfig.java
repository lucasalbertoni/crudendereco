package br.com.endereco.crud;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Classe criada para as configurações da aplicação.
 * <br /> Utilizando o padrão do Spring Boot.
 * @author Lucas
 *
 */
@SpringBootApplication
public class ApplicationConfig {
	
	/**
	 * Driver de conexão do banco de dados
	 */
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	
	/**
	 * URL de conexão do banco de dados.
	 */
	public static final String URL = "jdbc:mysql://localhost:3306/banco_prova";
	
	/**
	 * Usuário do banco de dados
	 */
	public static final String USERNAME = "root";
	
	/**
	 * Senha do banco de dados
	 */
	public static final String PASS = "";
	
	/**
	 * Método principal que sobe o servidor.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(ApplicationConfig.class, args);
	}
	
	/**
	 * Método de configurações de banco de dados.
	 * <br /> O ideal é buscar os dados de um arquivo criptografado para não expor o usuário de produção.
	 * @return
	 */
	@Bean
	public DataSource getDataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(DRIVER);
		dataSource.setUrl(URL);
		dataSource.setUsername(USERNAME);
		dataSource.setPassword(PASS);
		return dataSource;
	}
	
}
