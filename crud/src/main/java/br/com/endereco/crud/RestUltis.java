package br.com.endereco.crud;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;

/**
 * Classe de utilidades pertinentes para serviços REST.
 * @author Lucas
 *
 */
public class RestUltis {
	
	/**
	 * Auxilia para recuperar o erro retornado de um servidor REST.
	 */
	private static final String MESSAGE = "message";
	
	/**
	 * URL do serviço REST externo.
	 * <br /> Como melhoria, posteriormente, poderá ficar em um ENUM.
	 */
	public static final String URL_BUSCA_CEP = "http://localhost:8080/busca-cep?numeroCep=";
	
	/**
	 * Método responsável por recuperar a mensagem de erro de um serviço REST externo.
	 * @param e
	 * @return
	 */
	public static String getMensagemErroByRestClientError(RestClientException e){
		String mensagemErro = e.getMessage();
		try {
			JSONObject jsonObj = new JSONObject(((HttpServerErrorException)e).getResponseBodyAsString());
			mensagemErro = jsonObj.getString(MESSAGE);
		} catch (JSONException e1) {
			throw new RuntimeException(e1.getMessage());
		}
		return mensagemErro;
	}

}
