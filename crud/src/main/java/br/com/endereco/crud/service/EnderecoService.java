package br.com.endereco.crud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import br.com.endereco.crud.Utils;
import br.com.endereco.crud.controller.dto.Cep;
import br.com.endereco.crud.entity.Endereco;
import br.com.endereco.crud.service.clientws.EnderecoClientWS;
import br.com.endereco.crud.service.dao.EnderecoDao;

/**
 * Classe que oferece os serviços do endereço.
 * @author Lucas
 *
 */
@Service
public class EnderecoService {
	
	/**
	 * Atributo que faz comunicação com o banoc de dados.
	 */
	@Autowired
	private EnderecoDao enderecoDao;
	
	/**
	 * Atributo que faz comunicação externa necessário para completar o endereço.
	 */
	@Autowired
	private EnderecoClientWS enderecoClientWS;
	
	/**
	 * Método que fornece o serviço de buscar TODOS os endereços.
	 * @return
	 */
	public Iterable<Endereco> findAll(){
		Iterable<Endereco> enderecos = enderecoDao.findAll();
		return enderecos;
	}
	
	/**
	 * Método que fornece o serviço de buscar um endereço à partir de um ID.
	 * @param id
	 * @return
	 */
	public Endereco findOne(Long id){
		Endereco endereco = enderecoDao.findOne(id);
		return endereco;
	}
	
	/**
	 * Método que fornece o serviço de salvar um novo endereço.
	 * <br /> O ID DEVE ser null, é obrigatório preencher o número, CEP e 
	 * <br /> em alguns casos logradouro.
	 * <br /> Esse serviço é dependete do serviço externo de buscaCep.
	 * @param endereco
	 * @return
	 */
	public Endereco save(Endereco endereco){
		if(endereco == null || endereco.getId() != null){
			throw new BusinessException("Não foi possível efetuar a inclusão");
		}
		return saveOrUpdate(endereco);
	}
	
	/**
	 * Método que fornece o serviço de atualizar um endereço.
	 * <br /> O ID DEVE ser informado, é obrigatório preencher o número, CEP e 
	 * <br /> em alguns casos logradouro.
	 * <br /> Esse serviço é dependete do serviço externo de buscaCep.
	 * @param endereco
	 * @return
	 */
	public Endereco update(Endereco endereco){
		if(endereco == null || endereco.getId() == null){
			throw new BusinessException("Não foi possível efetuar a alteração");
		}
		saveOrUpdate(endereco);
		return endereco;
	}
	
	/**
	 * Método que fornece o serviço de excluir um endereço à partir de um ID.
	 * @param id
	 * @return
	 */
	public Long delete(Long id){
		if(Utils.isNullVazio(id)){
			throw new RestClientException("Não foi possível efetuar a exclusão");
		}
		Endereco endereco = new Endereco();
		endereco.setId(id);
		enderecoDao.delete(endereco);
		return id;
	}
	
	/**
	 * Método compartilhado entre o serviço de salvar e atualizar.
	 * @param endereco
	 * @return
	 */
	private Endereco saveOrUpdate(Endereco endereco){
		
		Cep cep = enderecoClientWS.buscaCep(endereco);
		mergeCepEndereco(endereco, cep);
		validarSaveOrUpdate(endereco);
		endereco = enderecoDao.save(endereco);
		return endereco;
	}

	/**
	 * Método com validações para salvar ou atualizar um endereço.
	 * @param endereco
	 */
	private void validarSaveOrUpdate(Endereco endereco) {
		if(Utils.isNullVazio(endereco.getLogradouro())){
			throw new BusinessException("Favor informar o logradouro");
		}
		if(Utils.isNullVazio(endereco.getCidade())){
			throw new BusinessException("Favor informar a cidade");
		}
		if(Utils.isNullVazio(endereco.getEstado())){
			throw new BusinessException("Favor informar o estado");
		}
	}

	/**
	 * Método responsável para preencher o endereço com os dados retornados do serviço de CEP.
	 * @param endereco
	 * @param cep
	 */
	private void mergeCepEndereco(Endereco endereco, Cep cep) {
		//Cidade
		if(Utils.isNotNullVazio(cep.getCidade()) && Utils.isNotNullVazio(cep.getCidade().getNome())){
			endereco.setCidade(cep.getCidade().getNome());
		}
		//Estado
		if(Utils.isNotNullVazio(cep.getCidade()) && Utils.isNotNullVazio(cep.getCidade().getEstado()) 
				&& Utils.isNotNullVazio(cep.getCidade().getEstado().getSigla().isEmpty())){
			endereco.setEstado(cep.getCidade().getEstado().getSigla());
		}
		//Bairro
		if(Utils.isNotNullVazio(cep.getBairro()) && Utils.isNotNullVazio(cep.getBairro().trim().isEmpty())){
			endereco.setBairro(cep.getBairro());
		}
		//Logradouro
		if(Utils.isNotNullVazio(cep.getLogradouro()) && Utils.isNotNullVazio(cep.getLogradouro().isEmpty())){
			endereco.setLogradouro(cep.getLogradouro());
		}
		//CEP
		if(Utils.isNotNullVazio(cep.getNumeroCep())){
			endereco.setCep(cep.getNumeroCep());
		}
	}

}
