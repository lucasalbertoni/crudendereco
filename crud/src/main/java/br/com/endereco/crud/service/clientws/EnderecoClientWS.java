package br.com.endereco.crud.service.clientws;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.endereco.crud.RestUltis;
import br.com.endereco.crud.Utils;
import br.com.endereco.crud.controller.dto.Cep;
import br.com.endereco.crud.entity.Endereco;
import br.com.endereco.crud.service.BusinessException;

/**
 * Classe para consumir serviços externos necessários para o Endereço.
 * @author Lucas
 *
 */
@Service
public class EnderecoClientWS {
	
	/**
	 * Método criado para buscar o CEP no serviço externo de BUSCA CEP.
	 * @param endereco
	 * @return
	 */
	public Cep buscaCep(Endereco endereco){
		validar(endereco);
		Cep cep = null;
		String url = RestUltis.URL_BUSCA_CEP+endereco.getCep();
		RestTemplate restTemplate = new RestTemplate();
		try{
			ResponseEntity<Cep> entity = restTemplate.getForEntity(url, Cep.class);
			cep = entity.getBody();
		}catch (RestClientException e) {
			throw new BusinessException(RestUltis.getMensagemErroByRestClientError(e));
		}
		return cep;
	}
	
	/**
	 * Método criado para validar se o endereço passado pode ser consultado no serviço externo.
	 * @param endereco
	 */
	private void validar(Endereco endereco){
		if(Utils.isNullVazio(endereco.getCep())){
			throw new BusinessException("Favor informar o CEP");
		}
		if(Utils.isNullVazio(endereco.getNumero())){
			throw new BusinessException("Favor informar o númaro");
		}
	}

}
