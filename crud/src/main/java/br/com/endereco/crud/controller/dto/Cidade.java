package br.com.endereco.crud.controller.dto;

import java.io.Serializable;

/**
 * Classe que representa a Cidade retornada de um serviço externo 
 * <br />Não é arquivo de persistencia, apenas de transferência de dados.
 * @author Lucas
 *
 */
public class Cidade implements Serializable{

	private static final long serialVersionUID = -8481050436676582765L;
	
	private String nome;
	private Estado estado;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
