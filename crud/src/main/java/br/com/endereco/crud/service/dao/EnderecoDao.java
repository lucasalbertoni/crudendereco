package br.com.endereco.crud.service.dao;

import org.springframework.data.repository.CrudRepository;

import br.com.endereco.crud.entity.Endereco;

/**
 * Interface que efetua a comunicação com o banco de dados.
 * <br /> Utilizando o repositório oferecido pelo Spring Boot.
 * @author Lucas
 *
 */
public interface EnderecoDao extends CrudRepository<Endereco, Long>{


}
