package br.com.endereco.crud.controller.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import br.com.endereco.crud.entity.Endereco;
import br.com.endereco.crud.service.BusinessException;
import br.com.endereco.crud.service.EnderecoService;

/**
 * Classe criada para expor os métodos de CRUD do endereço via REST.
 * <br /> Posteriormente necessário colocar mecanismos de segurança, 
 * <br /> por exemplo receber um token de autenticação.
 * @author Lucas
 *
 */
@RestController("/endereco")
public class EnderecoWS {
	
	/**
	 * Classe do serviço que será utilizado e que contém regras de negócio e acesso ao banco.
	 */
	@Autowired
	private EnderecoService enderecoService;
	
	/**
	 * Método que retorna TODOS os endereços (SEM FILTRO).
	 * @return
	 */
	@GetMapping("/endereco")
	public Iterable<Endereco> getFindAll(){
		try{
			Iterable<Endereco> enderecos = enderecoService.findAll();
			return enderecos;
		}catch (BusinessException e) {
			throw new RestClientException(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			throw new RestClientException("Erro na consulta");
		}
	}
	
	/**
	 * Método que retorna um endereço através do ID passado.
	 * @param id
	 * @return
	 */
	@GetMapping("/endereco/{id}")
	public Endereco getById(@PathVariable Long id){
		try{
			Endereco endereco = enderecoService.findOne(id);
			return endereco;
		}catch (BusinessException e) {
			throw new RestClientException(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			throw new RestClientException("Erro na consulta");
		}
	}
	
	/**
	 * Método que cria um novo endereço.
	 * @param endereco
	 * @return
	 */
	@PostMapping
	public Endereco create(@RequestBody Endereco endereco){
		try{
			return enderecoService.save(endereco);
		}catch (BusinessException e) {
			throw new RestClientException(e.getMessage());
		}catch (Exception e){
			e.printStackTrace();
			throw new RestClientException("Não foi possível efetuar a inclusão");
		}
	}
	
	/**
	 * Método utilizado para atualizar um endereço existente.
	 * @param endereco
	 * @return
	 */
	@PutMapping
	public Endereco update(@RequestBody Endereco endereco){
		try{
			return enderecoService.update(endereco);
		}catch (BusinessException e) {
			throw new RestClientException(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			throw new RestClientException("Erro na alteração");
		}
	}
	
	/**
	 * Método responsável por excluir um endereço existente pelo ID informado.
	 * @param id
	 * @return
	 */
	@DeleteMapping("/endereco/{id}")
	public Long delete(@PathVariable Long id){
		try{
			return enderecoService.delete(id);
		}catch (BusinessException e) {
			throw new RestClientException(e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			throw new RestClientException("Erro ao deletar");
		}
	}

}
