package br.com.endereco.crud.service;

/**
 * Classe que representa erro de negócio, ou seja, erros esperados.
 * @author Lucas
 *
 */
public class BusinessException extends RuntimeException{

	private static final long serialVersionUID = 4683820481113342002L;
	
	/**
	 * Classe construtora.
	 * @param error
	 */
	public BusinessException(String error){
		super(error);
	}

}
