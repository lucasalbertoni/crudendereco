package br.com.endereco.crud.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Classe para persistência no banco de dados.
 * @author Lucas
 *
 */
@Entity(name="endereco")
public class Endereco implements Serializable{

	private static final long serialVersionUID = -1696151755987168763L;
	
	@Id
	@GeneratedValue
	private Long id;
	private String cep;
	private String logradouro;
	private String bairro;
	private String numero;
	private String complemento;
	private String cidade;
	private String estado;
	
	@Override
	public boolean equals(Object obj) {
		return this.id != null && ((Endereco)obj).getId() != null && this.id.equals(((Endereco)obj).getId());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
