package br.com.endereco.crud;

import java.util.List;

/**
 * Classe de utilidades que são pertinentes em muitos pontos do sistema.
 * @author Lucas
 *
 */
public class Utils {

	/**
	 * Validar se o objeto passado é nulo ou vazio.
	 * <br />Se for NULL ou Vazio Retorna TRUE.
	 * @param objeto
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isNullVazio(Object objeto){
		if(objeto instanceof String){
			String teste = (String) objeto;
			return teste == null || teste.trim().isEmpty();
		}else if(objeto instanceof List){
			List teste = (List) objeto;
			return teste == null || teste.size() == 0;
		}else{
			return objeto == null;
		}
	}
	
	/**
	 * Método utilizado para validar se o objeto não é NULL nem VAZIO.
	 * <br /> se não for NULL nem VAZIO retorna TRUE.
	 * @param objeto
	 * @return
	 */
	public static boolean isNotNullVazio(Object objeto){
		return !isNullVazio(objeto);
	}
}
